# -*- coding: utf-8 -*-
list1 = [1,2,3,4,5,6,7,8]

print(list1[::-2])
print(max(list1))

#%% Quiz 7.1
n1 = int(input("Number of elements in first list = "))
vet1 = []

for t in range(n1):
    cur = int(input(f'l1[{t}]='))
    vet1.append(cur)
vet1 = set(vet1)

n2 = int(input("Number of elements in second list = "))
vet2 = []

for t in range(n2):
    cur = int(input(f'l2[{t}]='))
    vet2.append(cur)
vet2 = set(vet2)

l3 = vet1.intersection(vet2)
l3 = list(l3)
l3.sort()

print(l3)
#%% Quiz 7.2
n1 = int(input())
vet1 = []

for t in range(n1):
    cur = int(input())
    vet1.append(cur)
set1 = set(vet1)

dist = n1 - (len(vet1) - len(set1))

print(f'number of distinct values: {dist}')
#%% Quiz 7.3
n1 = int(input("Number of elements in first list = "))
vet1 = []

for t in range(n1):
    cur = int(input(f'l1[{t}]='))
    vet1.append(cur)


n2 = int(input("Number of elements in second list = "))
vet2 = []

for t in range(n2):
    cur = int(input(f'l2[{t}]='))
    vet2.append(cur)

l3 = vet1 + vet2

print(l3)