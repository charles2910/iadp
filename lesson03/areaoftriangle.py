"""
Created on 2021

@author: António Brito / Carlos Bragança

"""
base = float(input("Base of the triangle = "))
height = float(input("Height of the triangle = "))
area = base * height / 2
print(f"Area of the triangle = {area:.2f}")
