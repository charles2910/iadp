# -*- coding: utf-8 -*-
#%% Quiz 11.1
fn = input("Filename:")
fp = open(fn)
count = 0
sum_val = 0.0

for line in fp:
    value = float(line.strip())
    sum_val += value
    count += 1

mean = sum_val / count

print(f"Mean = {mean:0.2f}")

fp.close()

#%% Quiz 11.2
filename1 = input("Filename 1:")
filename2 = input("Filename 2:")
f1 = open(filename1)

for line1 in f1:
    f2 = open(filename2)
    for line2 in f2:
        name = line1.strip()
        surname = line2.strip()
        print(f'{name} {surname}')
    f2.close()
f1.close()

#%% Quiz 11.3

filename = input("Filename:")
column = input("Column name:")
function = input("Function name:")
fh = open(filename)
head = fh.readline()
headlist = [header for header in head.split(";")]
findex = headlist.index(column)
headcount = len(headlist)
vallist = [[header] for header in headlist]
head = fh.readline()
while head != "":
    l = head.split(";")
    for i in range(len(l)):
        vallist[i].append(l[i])
    head = fh.readline()

vallist[findex] = vallist[findex][1::]
for i in range(len(vallist[findex])):
    vallist[findex][i] = int(vallist[findex][i])
if function == "max":
    result = float(max(vallist[findex]))
if function == "min":
    result = float(min(vallist[findex]))
if function == "sum":
    result = float(sum(vallist[findex]))

#%% Quiz 11.3 alt
filename = input("Filename:")
cn = input("Column name:")
fn = input("Function name:")

fp = open(filename)

head = fp.readline().strip().split(";")

v_max = 0
v_min = 200000000
v_sum = 0
count = 0

column = head.index(cn)

for line in fp:
    values = line.strip().split(';')
    v_int = int(values[column])
    if v_int > v_max:
        v_max = v_int
    if v_int < v_min:
        v_min = v_int
    v_sum += v_int
    count += 1

fp.close()

if fn == 'mean':
    v_mean = v_sum / count
    print(f'{fn}({cn})={v_mean:.2f}')
if fn == 'sum':
    print(f'{fn}({cn})={v_sum:.2f}')
if fn == 'max':
    print(f'{fn}({cn})={v_max:.2f}')
if fn == 'min':
    print(f'{fn}({cn})={v_min:.2f}')
