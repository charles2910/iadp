"""
    Exercício 1 - Aula 4
    
    Feito por: Carlos H L Melara 2022
    
    Licença GPL3+
"""

n1 = int(input("Primeiro Número: "))
n2 = int(input("Segundo Número: "))

if n1 > n2:
    print('O maior número é o primeiro: ', n1)
else:
    print('O maior número é o segundo1: ', n2)