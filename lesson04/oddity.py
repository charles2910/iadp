"""
    Exercício 2 - Aula 4
    
    Feito por: Carlos H L Melara 2022
    
    Licença GPL3+
"""

n1 = int(input("Número: "))

if n1 % 2 == 1:
    print('O número é ímpar')
else:
    print('O número é par ')