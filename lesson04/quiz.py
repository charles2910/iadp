"""
    Quiz - Aula 4
    
    Feito por: Carlos H L Melara 2022
    
    Licença GPL3+
    
    Enunciado: Given an integer with 3 digits, compute all the numbers that can
               be formed using those digits.
"""
#%%
number = input()

n1 = number[0]
n2 = number[1]
n3 = number[2]

print('Numbers:')
print(n1+n2+n3)
print(n1+n3+n2)
print(n2+n3+n1)
print(n2+n1+n3)
print(n3+n2+n1)
print(n3+n1+n2)

#%%
base = float(input())
height = float(input())
area = base * height / 2
print(f"Area of the triangle = {area:.2f}")

#%%

day = int(input())
waiting_period = int(input())

weeks_left = waiting_period // 5
end_day = (day + waiting_period % 5) % 5

if end_day == 0:
    end_day = 5

print(f'weeks: {weeks_left}, workday: {end_day}')
