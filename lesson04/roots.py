"""
    Exercício 3'a' - Aula 4
    
    Feito por: Carlos H L Melara 2022
    
    Licença GPL3+
"""

import math

mensagem_inicial = """Esse programa calcula as raízes de uma equação de 2º grau

Coloque os índices da equação da seguinte forma: a x^2 + b x + c
"""

print(mensagem_inicial)
a = float(input("a: "))
b = float(input("b: "))
c = float(input("c: "))

squared = b*b - 4 * a * c

if squared >= 0:
    sqrt = math.sqrt(squared)
    r1 = - b + sqrt / (2 * a)
    r2 = - b - sqrt / (2 * a)
    print(f'As raízes são {r1:.2f} e {r2:.2f}')
else:
    print('Não há raízes reais')