"""
    Exercício 1 - Aula 5
    
    Feito por: Carlos H L Melara 2022
    
    Licença GPL3+

"""

number = int(input('Calcular o fatorial de: '))
fat = number

while number - 1 >= 2:
    number = number - 1
    fat = fat * number

print('Fatorial: ', fat)