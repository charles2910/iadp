"""
Created on 2021

@author: António Brito / Carlos Bragança

"""
#%%
i = 1
while i <= 10:
    print(i)
    i = i + 1
print("End")

#%% ex mean of a list of n values
n = int(input("Number of values = "))
sumv = 0
i = 1
while i <= n:
    value = float(input("value="))
    sumv = sumv + value
    i = i + 1
mean = sumv / n
print("Mean = ", mean)

#%% Quiz 5.1
n = int(input("n:"))
fact = 1
while n > 1:
    fact = n * fact
    n -= 1
print(f'Factorial = {fact}')
#%% Quiz 5.2
n = int(input("n:"))
t = 1
vet = []
maxv = 0
maxi = 0
cur = 0

while t <= n:
    cur = int(input(f'value {t}:'))
    vet.append(cur)
    if cur > maxv:
        maxv = cur
        maxi = t
    t += 1

print(f'Maximum = {maxv} position = {maxi}')
#%% Quiz 5.3
n = int(input("N:"))

prime = [2]
cur = 3
mightBePrime = True
if n > 0:
    print(2)

while n >= 2:
    for x in prime:
        if(cur % x == 0):
            mightBePrime = False
    if (mightBePrime):
        prime.append(cur)
        print(cur)
        n -= 1
    mightBePrime = True
    cur += 1
#%% Quiz 5.3 sem for
n = int(input("N:"))

prime = [2]
cur = 3
mightBePrime = True
t = 0

if n > 0:
    print(2)

while n >= 2:
    while t < len(prime):
        if(cur % prime[t] == 0):
            mightBePrime = False
        t += 1
    if (mightBePrime):
        prime.append(cur)
        print(cur)
        n -= 1
    mightBePrime = True
    cur += 1
    t = 0
    

    














