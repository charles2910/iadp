# -*- coding: utf-8 -*-

#%% Quiz 10.1
def senx(x, n):
    sen = 0.0
    while x > 3.1415926535:
        x -= 3.14159265358
    for i in range(1, n + 1):
        n1 = pow(-1, i + 1)
        n2 = pow(x, 2 * i - 1)
        n3 = fat(2 * i - 1)

        sen += n1 * n2 / n3
    return round(sen, 4)

def fat(n):
    fat = n
    while n - 1 >= 2:
        n = n - 1
        fat = fat * n
    return fat

#%% Quiz 10.2
def average(l1):
    mean = sum(l1)/len(l1)
    return round(mean, 1)

#%% Quiz 10.3

def iscons(char):
    char = str(char)
    char = char.lower()
    if char.isalpha():
        if char in ['a', 'e', 'i', 'o', 'u']:
            return False
        else:
            return True
    else:
        return False