# -*- coding: utf-8 -*-

# quiz 11.1
s1 = input()
s1 = s1.lower()
n = 0

for i in s1:
    if i in ['a', 'e', 'i', 'o', 'u']:
        n += 1

print(f'N. of vowels = {n}')

#%% quiz 11.2

s1 = input()
s2 = []

s2.append(s1[-3])
s2.append(s1[-2])
s2.append(s1[-1])
if len(s1) > 3:
    for t in s1[-3::-1]:
        s2.append(t)
s2.reverse()

for i in s2:
    print(i, sep='', end='')

#%% quiz 11.3

s1 = input()
n = int(input())
s1 = s1.upper()
s2 = []


for t in s1:
    newl = ord(t) + n
    if newl > 90:
        newl -= 26
    s2.append(chr(newl))

for i in s2:
    print(i, sep='', end='')
