# -*- coding: utf-8 -*-

#%% Quiz 12.1
n = int(input())

a = [[0] * n for i in range(n)]

for i in range(n):
    for j in range(n):
        a[i][j] = input()
    a[i].sort()

for i in range(n):
    print(a[i])

#%% Quiz 12.2
n = int(input())
m = int(input())

a = [[0] * m for i in range(n)]
b = [[0] * (m + 1) for i in range(n + 1)]

for i in range(n):
    for j in range(m):
        a[i][j] = float(input())
        b[i][j] = a[i][j]
    b[i][m] = sum(a[i])

for j in range(m + 1):
    for i in range(n):
        if j < m:
            b[n][j] += a[i][j]
        else:
            b[n][m] += b[i][j]

for i in range(n + 1):
    print(b[i])
    
#%% Quiz 12.3
n = int(input())
max1 = 0
max1_idx = [0 * 2]
max2_idx = [0 * 2]
max2 = 0
a = [[0] * n for i in range(n)]

for j in range(n):
    for i in range(n):
        a[i][j] = int(input())

for i in range(n):
    for j in range(n):
        cur = a[i][j]
        if cur > max1:
            max2 = max1
            max2_idx = max1_idx
            max1 = cur
            max1_idx = [i, j]
        elif cur > max2:
            max2 = cur
            max2_idx = [i, j]
print(f'2nd maximum = {max2:.2f}, line = {max2_idx[0]}, column = {max2_idx[1]}')