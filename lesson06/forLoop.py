# -*- coding: utf-8 -*-

tot = 0
for i in range(1,3):
    for k in range(1,3):
        tot = tot + i*k
print(tot)

#%%
num = 10
for i in range(10):
    for k in range(2, 10, 1):
        if num % 2 == 0:
            continue
            num += 1
        else:
            num += 1
    num += 1
print(num)

#%%
for num in range(-1,-3,-1):
    print(num, end=", ")
print('End')
#%%
for i in range(1,3):
    for k in range(1,3):
        tot = tot + i*k
print(tot)

#%% Quiz 6.1
n = int(input("n:"))
fact = 1
for i in range(n, 1, -1):
    fact = i * fact
print(f'Factorial = {fact}')
#%% Quiz 6.2
n = int(input("n:"))
vet = []
maxv = 0
cur = 0

for t in range(1, n + 1):
    cur = int(input(f'value {t}:'))
    vet.append(cur)
    if t > 1 and abs(cur - vet[t - 2]) > maxv:
        maxv = abs(cur - vet[t - 2])

print(f'Maximum difference = {maxv:.2f}')
#%%
n = int(input("n:"))
vet = []
max1 = 0
max2 = 0
cur = 0

for t in range(1, n + 1):
    cur = int(input(f'value {t}:'))
    vet.append(cur)
    if cur > max1:
        max2 = max1
        max1 = cur
    elif cur > max2:
        max2 = cur

print(f'Highest value = {max1} 2nd. Highest value = {max2}')






